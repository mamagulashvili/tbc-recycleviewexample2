package com.example.tbcrecycleviewexample2

data class ProductModel(
    val name:String,
    val price:String,
    val image:String,
    val amount:String? = null
)