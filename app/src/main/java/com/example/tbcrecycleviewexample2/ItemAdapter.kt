package com.example.tbcrecycleviewexample2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.tbcrecycleviewexample2.databinding.AvailableProductItemBinding
import com.example.tbcrecycleviewexample2.databinding.NotAvailableProductItemBinding

class ItemAdapter(
    private val productList: MutableList<ProductModel>,val onItemClickListener: OnItemListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val AVAILABLE_PRODUCT = 1
        private const val NOT_AVAILABLE_PRODUCT = 2
    }

    private lateinit var productModel: ProductModel

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == AVAILABLE_PRODUCT) {
            ProductViewHolder(
                AvailableProductItemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        } else {
            NotAvailableProductViewHolder(
                NotAvailableProductItemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ProductViewHolder -> holder.onBind()
            is NotAvailableProductViewHolder -> holder.onBind()
        }
    }

    override fun getItemCount(): Int = productList.size


    inner class ProductViewHolder(val binding: AvailableProductItemBinding) :
        RecyclerView.ViewHolder(binding.root) , View.OnClickListener,View.OnLongClickListener{
        fun onBind() {
            productModel = productList[adapterPosition]
            binding.apply {
                tvProductName.text = productModel.name
                tvPrice.text = productModel.price
                tvAmount.text = productModel.amount ?: "0"
                btnAddToCart.setOnClickListener(this@ProductViewHolder)
                root.setOnLongClickListener(this@ProductViewHolder)
            }
            itemView.apply {
                Glide.with(this).load(productModel.image).into(binding.ivProduct)
            }
        }

        override fun onClick(v: View?) {
            productModel = productList[adapterPosition]
            onItemClickListener.onItemClickListener(productModel.name,"Added to Cart")
        }

        override fun onLongClick(v: View?): Boolean {
            onItemClickListener.onItemLongClickListener(adapterPosition)
            return true
        }
    }

    inner class NotAvailableProductViewHolder(val binding: NotAvailableProductItemBinding) :
        RecyclerView.ViewHolder(binding.root),View.OnLongClickListener,View.OnClickListener {
        fun onBind() {

            binding.apply {
                productModel = productList[adapterPosition]
                tvProductName.text = productModel.name
                tvPrice.text = productModel.price
                btnAddToCart.setOnClickListener(this@NotAvailableProductViewHolder)
                root.setOnLongClickListener(this@NotAvailableProductViewHolder)
            }
            itemView.apply {
                Glide.with(this).load(productModel.image).into(binding.ivProduct)
            }
        }

        override fun onLongClick(v: View?): Boolean {
            onItemClickListener.onItemLongClickListener(adapterPosition)
            return true
        }

        override fun onClick(v: View?) {
            productModel = productList[adapterPosition]
            onItemClickListener.onItemClickListener(productModel.name,"Is Not Available")
        }
    }

    override fun getItemViewType(position: Int): Int {
        val item = productList[position]
        return if (item.amount == null)
            NOT_AVAILABLE_PRODUCT
        else
            AVAILABLE_PRODUCT
    }
}