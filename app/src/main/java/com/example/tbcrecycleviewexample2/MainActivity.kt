package com.example.tbcrecycleviewexample2

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tbcrecycleviewexample2.databinding.ActivityMainBinding
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val productList = mutableListOf<ProductModel>()
    private lateinit var itemAdapter: ItemAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        setRecycleView()
        setData()
    }

    private fun setRecycleView() {
        val onItemListener = object : OnItemListener {
            override fun onItemClickListener(name: String, message: String) {
                showDialog(name, message)
            }

            override fun onItemLongClickListener(position: Int) {
                removeItem(position)
            }

        }
        itemAdapter = ItemAdapter(productList, onItemListener)

        binding.rvProduct.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = itemAdapter
        }
    }
    private fun removeItem(position:Int){
        val item = productList[position]
        productList.removeAt(position)
        itemAdapter.notifyItemRemoved(position)
        Snackbar.make(binding.root,"Item Successfully Deleted",Snackbar.LENGTH_SHORT).apply {
            setAction("Undo"){
                productList.add(position,item)
                itemAdapter.notifyItemInserted(position)
            }
        }.show()
    }

    private fun showDialog(name: String, message: String) {
        val builder = AlertDialog.Builder(this).apply {
            setTitle(name)
            setMessage("This Product $message")
        }.create()
        builder.show()
    }

    private fun setData() {
        productList.apply {
            add(
                ProductModel(
                    "Intel Core i7-9700K Desktop Processor",
                    "270$",
                    "https://images-na.ssl-images-amazon.com/images/I/71Q5sdPHD-L._AC_SL1500_.jpg",
                    "7"
                )
            )
            add(
                ProductModel(
                    "Intel Core i9-9900K Desktop Processor",
                    "450$",
                    "https://images-na.ssl-images-amazon.com/images/I/61qUfPKfqJL._AC_SL1200_.jpg"
                )
            )
            add(
                ProductModel(
                    "MSI Gaming GeForce GTX 1650",
                    "630$",
                    "https://images-na.ssl-images-amazon.com/images/I/61rzWyQBAoL._AC_SL1024_.jpg",
                )
            )
            add(
                ProductModel(
                    "Redragon K530",
                    "60$",
                    "https://images-na.ssl-images-amazon.com/images/I/61IhHiYBiyL._AC_SL1500_.jpg",
                    "10"
                )
            )
            add(
                ProductModel(
                    "ASUS GeForce GTX 1050 Ti 4GB",
                    "370$",
                    "https://images-na.ssl-images-amazon.com/images/I/61TgBrsiRKS._AC_SL1000_.jpg",
                    "2"
                )
            )
            add(
                ProductModel(
                    "Ghost M1 Mouse ",
                    "70$",
                    "https://images-na.ssl-images-amazon.com/images/I/71kK1wOERsL._AC_SL1500_.jpg",
                )
            )
            add(
                ProductModel(
                    "Intel Core i5-10600K Desktop Processor",
                    "205$",
                    "https://images-na.ssl-images-amazon.com/images/I/61xAzEoCuhL._AC_SL1223_.jpg",
                    "5"
                )
            )
        }
        itemAdapter.notifyDataSetChanged()
    }
}