package com.example.tbcrecycleviewexample2

interface OnItemListener {
    fun onItemClickListener(name:String,message:String)
    fun onItemLongClickListener(position:Int)
}